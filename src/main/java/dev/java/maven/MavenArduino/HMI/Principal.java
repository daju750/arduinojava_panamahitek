package dev.java.maven.MavenArduino.HMI;

import javax.swing.*;
import java.awt.event.*;

public class Principal extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JButton boton1,boton2,boton3;
	
    public Principal() {
        setLayout(null);
        boton1=new JButton("1");
        boton1.setBounds(10,100,90,30);
        add(boton1);
        boton1.addActionListener(this);
        boton2=new JButton("2");
        boton2.setBounds(110,100,90,30);
        add(boton2);
        boton2.addActionListener(this);
        boton3=new JButton("3");
        boton3.setBounds(210,100,90,30);
        add(boton3);
        boton3.addActionListener(this); 
        
    }
    
    public void actionPerformed(ActionEvent e) {
    	if (e.getSource()==boton1) {
            setTitle("boton 1");
        }
        if (e.getSource()==boton2) {
            setTitle("boton 2");
        }
        if (e.getSource()==boton3) {
            setTitle("boton 3");
        }  
    }
    
    public static void main(String[] ar) {
        Principal formulario1=new Principal();
        formulario1.setBounds(0,0,450,350);
        formulario1.setLocationRelativeTo(null);
        formulario1.setName("Meclazdora de Pintura");
        
        formulario1.setVisible(true);
    }
    
}